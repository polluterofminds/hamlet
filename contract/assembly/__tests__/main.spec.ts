// import { get_contract_owner, init, mint_to, get_token_uri, get_token_owner, grant_access, check_access, revoke_access, transfer_from, change_metadata_uri, get_token_id_by_owner } from '../index';
import { get_contract_owner, init, mint_to, get_token_owner, get_token_id_by_owner, get_token_uri, change_metadata_uri, change_subscription_price, get_subscription_price, grant_access, check_access, revoke_access, transfer_from, mint_subscription_to, get_token_id_by_subscription_owner } from '../index';
import { VMContext, u128, logging } from "near-sdk-as";
import { TokenOwner } from '../models';
const ALICE = 'alice'
const BOB = 'bob'
const CAROL = 'carol'
const METADATA_URI = "ipfs://QmPhruW8YK6PpvchQokowg1X5kH14Z1xSZFfto44Gvx2gS"

describe("Initializing Contract", () => {
    it("should successfully initialize the contract", () => {
        //  We do this because each user will be deploying their own contract and this represents a user logged in deploying
        VMContext.setPredecessor_account_id(ALICE)
        init()
        const owner = get_contract_owner()
        expect(owner).toBe(ALICE)
    })
    it("should not allow init to be called after contract is deployed", () => {
        //  We do this because each user will be deploying their own contract and this represents a user logged in deploying
        VMContext.setPredecessor_account_id(ALICE)
        init()
        expect(() => {
            VMContext.setPredecessor_account_id(BOB)
            init()
        }).toThrow()
    })
})

describe("Minting", () => {
    it("should get a null owner", () => {
        const owner = get_contract_owner()
        expect(owner).toBe(null)
    });
    it("should get correct owner", () => {
        VMContext.setPredecessor_account_id(ALICE)
        init()
        const owner = get_contract_owner()
        expect(owner).toBe(ALICE)
    });
    it("should mint a new publication owner token", () => {
        VMContext.setPredecessor_account_id(ALICE)
        init()
        VMContext.setPredecessor_account_id(BOB)
        const METADATA_URI = "Qm093..."
        const subscriptionPrice = 1;
        VMContext.setPredecessor_account_id(CAROL)
        const tokenId = mint_to(CAROL, METADATA_URI, subscriptionPrice)
        expect(tokenId).toBe(1);
    });
    it("should get correct token owner", () => {
        VMContext.setPredecessor_account_id(ALICE)
        init()
        VMContext.setPredecessor_account_id(BOB)
        const METADATA_URI = "Qm093..."
        const subscriptionPrice = 1;
        VMContext.setPredecessor_account_id(CAROL)
        const tokenId = mint_to(CAROL, METADATA_URI, subscriptionPrice)
        expect(tokenId).toBe(1);
        const owner = get_token_owner(tokenId)
        expect(owner).toBe(CAROL)
    })
    it("should get correct token id based on token owner", () => {
        VMContext.setPredecessor_account_id(ALICE)
        init()
        logging.log('here we go');
        VMContext.setPredecessor_account_id(BOB)
        const METADATA_URI = "Qm093..."
        const subscriptionPrice = 1;
        VMContext.setPredecessor_account_id(CAROL)
        const tokenId = mint_to(CAROL, METADATA_URI, subscriptionPrice)
        expect(tokenId).toBe(1);
        const tokens = get_token_id_by_owner(CAROL);

        let tokenMap = new Array<TokenOwner>();
        let newToken = new TokenOwner(tokenId, CAROL, subscriptionPrice, METADATA_URI);
        tokenMap.push(newToken);
        expect(tokens).toStrictEqual(tokenMap);
    })
    it("should get the tokenURI", () => {
        VMContext.setPredecessor_account_id(ALICE)
        init()
        VMContext.setPredecessor_account_id(BOB)
        const METADATA_URI = "Qm093..."
        const subscriptionPrice = 1;
        VMContext.setPredecessor_account_id(CAROL)
        const tokenId = mint_to(CAROL, METADATA_URI, subscriptionPrice)
        expect(tokenId).toBe(1);
        const owner = get_token_owner(tokenId)
        expect(owner).toBe(CAROL)
        const metadataUri = get_token_uri(tokenId)
        expect(metadataUri).toBe(METADATA_URI)
    })
    it("should change the tokenURI", () => {
        VMContext.setPredecessor_account_id(ALICE)
        init()
        VMContext.setPredecessor_account_id(BOB)
        const METADATA_URI = "Qm093..."
        const subscriptionPrice = 1;
        VMContext.setPredecessor_account_id(CAROL)
        const tokenId = mint_to(CAROL, METADATA_URI, subscriptionPrice)
        expect(tokenId).toBe(1);
        const owner = get_token_owner(tokenId)
        expect(owner).toBe(CAROL)
        const metadataUri = get_token_uri(tokenId)
        expect(metadataUri).toBe(METADATA_URI)
        const METADATA_2 = "Qm2";
        change_metadata_uri(tokenId, METADATA_2);
        const newMetadataUri = get_token_uri(tokenId)
        expect(newMetadataUri).toBe(METADATA_2)
    })
    it("should change the subscription price", () => {
        VMContext.setPredecessor_account_id(ALICE)
        init()
        VMContext.setPredecessor_account_id(BOB)
        const METADATA_URI = "Qm093..."
        const subscriptionPrice = 1;
        VMContext.setPredecessor_account_id(CAROL)
        const tokenId = mint_to(CAROL, METADATA_URI, subscriptionPrice)
        expect(tokenId).toBe(1);
        const owner = get_token_owner(tokenId)
        expect(owner).toBe(CAROL)
        const newPrice = 2;
        change_subscription_price(tokenId, newPrice);
        const updatedPrice = get_subscription_price(tokenId);
        expect(updatedPrice).toBe(newPrice);
    })
})

describe("Access", () => {
    it("should successfuly grant token acces to another user", () => {
        VMContext.setPredecessor_account_id(ALICE)
        init()
        VMContext.setPredecessor_account_id(BOB)
        const METADATA_URI = "Qm093..."
        const subscriptionPrice = 1;
        VMContext.setPredecessor_account_id(CAROL)
        mint_to(CAROL, METADATA_URI, subscriptionPrice)
        grant_access(ALICE)
        VMContext.setPredecessor_account_id(ALICE)
        expect(check_access(CAROL)).toBe(true)
    });
    it("should revoke access for a user", () => {
        VMContext.setPredecessor_account_id(ALICE)
        init()
        VMContext.setPredecessor_account_id(BOB)
        const METADATA_URI = "Qm093..."
        const subscriptionPrice = 1;
        VMContext.setPredecessor_account_id(CAROL)
        mint_to(CAROL, METADATA_URI, subscriptionPrice)
        grant_access(ALICE)
        revoke_access(ALICE)
        VMContext.setPredecessor_account_id(ALICE)
        expect(check_access(CAROL)).toBe(false)
    })
});

describe("Transfers", () => {
    it("should transfer token owner by Alice to Bob", () => {
        VMContext.setPredecessor_account_id(ALICE)
        init()
        VMContext.setPredecessor_account_id(CAROL)
        const METADATA_URI = "Qm093..."
        const subscriptionPrice = 1;
        const carolToken = mint_to(CAROL, METADATA_URI, subscriptionPrice)
        expect(get_token_owner(carolToken)).toBe(CAROL)
        expect(get_token_owner(carolToken)).not.toBe(BOB)

        transfer_from(CAROL, BOB, carolToken)

        expect(get_token_owner(carolToken)).toBe(BOB)
        expect(get_token_owner(carolToken)).not.toBe(CAROL)
    })
    it("allows escrowed tokens to be transferred by escrowee", () => {
        VMContext.setPredecessor_account_id(ALICE)
        init()
        VMContext.setPredecessor_account_id(CAROL)
        const METADATA_URI = "Qm093..."
        const subscriptionPrice = 1;
        const carolToken = mint_to(CAROL, METADATA_URI, subscriptionPrice);
        expect(get_token_owner(carolToken)).toBe(CAROL)
        expect(get_token_owner(carolToken)).not.toBe(BOB)

        // BOB transfers to himself
        VMContext.setPredecessor_account_id(BOB)
        transfer_from(CAROL, BOB, carolToken)

        expect(get_token_owner(carolToken)).toBe(BOB)
        expect(get_token_owner(carolToken)).not.toBe(CAROL)
    })
});

describe("subscription minting", () => {
    it("should mint a new subscription token", () => {
        VMContext.setPredecessor_account_id(ALICE)
        init()
        VMContext.setPredecessor_account_id(CAROL)
        const METADATA_URI = "Qm093..."
        const subscriptionPrice = 1;
        const pubToken = mint_to(CAROL, METADATA_URI, subscriptionPrice);
        expect(get_token_owner(pubToken)).toBe(CAROL)
        VMContext.setPredecessor_account_id(BOB)
        VMContext.setAttached_deposit(u128.from('1'));
        const subscriptionURI = "SUB_URI";
        const tokenId = mint_subscription_to(BOB, pubToken, subscriptionURI)
        expect(tokenId).toBe(2);
    });

    it("should not allow a token to be minted if the attached deposit is too low", () => {        
        expect(() => {
            VMContext.setPredecessor_account_id(ALICE)
            init()
            VMContext.setPredecessor_account_id(CAROL)
            const METADATA_URI = "Qm093..."
            const subscriptionPrice = 2;
            const pubToken = mint_to(CAROL, METADATA_URI, subscriptionPrice);
            expect(get_token_owner(pubToken)).toBe(CAROL)
            VMContext.setPredecessor_account_id(BOB)
            const subscriptionURI = "SUB_URI";
            VMContext.setAttached_deposit(u128.from('1'));
            const tokenId = mint_subscription_to(BOB, pubToken, subscriptionURI)
            expect(tokenId).toBe(2);
        }).toThrow()
    });
    it("should get the correct token id based on owner", () => {
        VMContext.setPredecessor_account_id(ALICE)
        init()
        VMContext.setPredecessor_account_id(CAROL)
        const METADATA_URI = "Qm093..."
        const subscriptionPrice = 1;
        const pubToken = mint_to(CAROL, METADATA_URI, subscriptionPrice);
        expect(get_token_owner(pubToken)).toBe(CAROL)
        VMContext.setPredecessor_account_id(BOB)
        VMContext.setAttached_deposit(u128.from('1'));
        const subscriptionURI = "SUB_URI";
        const tokenId = mint_subscription_to(BOB, pubToken, subscriptionURI)
        expect(tokenId).toBe(2);
        const subscriptionOwnerTokens = get_token_id_by_subscription_owner(BOB) 
        expect(subscriptionOwnerTokens.length).toBe(1);
        expect(subscriptionOwnerTokens[0].publication_token_id).toBe(1);
    })
})
