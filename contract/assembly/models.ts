@nearBindgen
export class TokenOwner {
  constructor(
    public token_id: u64,
    public owner: string,
    public subscriptionPrice: u64,
    public metadata_uri: string) { }
}

@nearBindgen
export class SubscriptionTokenOwner {
  constructor(
    public token_id: u64,
    public owner: string,
    public publication_token_id: u64, 
    public metadata_uri: string) { }
}