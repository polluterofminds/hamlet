import { PersistentMap, storage, context, u128, PersistentVector, logging, ContractPromiseBatch } from 'near-sdk-as'
import { SubscriptionTokenOwner, TokenOwner } from "./models";

type AccountId = string
type TokenId = u64
type MetadataUri = string

const tokenToOwner = new PersistentMap<TokenId, AccountId>('a')
const tokenToMetadata = new PersistentMap<TokenId, MetadataUri>('b')
const ownerToToken = new PersistentVector<TokenOwner>('c')
const subscriptionOwnerToToken = new PersistentVector<SubscriptionTokenOwner>('d')
const escrowAccess = new PersistentMap<AccountId, AccountId>('e')
const TOTAL_SUPPLY = 'f'

export const ERROR_CONTRACT_ALREADY_INITIALIZED = 'Contract has already been initialized'
export const ERROR_NO_ESCROW_REGISTERED = 'Caller has no escrow registered'
export const ERROR_CALLER_ID_DOES_NOT_MATCH_EXPECTATION = 'Caller ID does not match expectation'
export const ERROR_OWNER_IS_NULL = 'Owner is null'
export const ERROR_MAXIMUM_TOKEN_LIMIT_REACHED = 'Maximum token limit reached'
export const ERROR_OWNER_ID_DOES_NOT_MATCH_EXPECTATION = 'Owner id does not match real token owner id'
export const ERROR_TOKEN_NOT_OWNED_BY_CALLER = 'Token is not owned by the caller. Please use transfer_from for this scenario'
export const ERROR_DEPOSIT_AMOUNT_INVALID = 'Deposited amount is not valid'

export function init(): void {
  assert(storage.get<string>("init") == null, ERROR_CONTRACT_ALREADY_INITIALIZED);
  assert(storage.get<string>("contract_owner") == null, ERROR_CONTRACT_ALREADY_INITIALIZED)
  const predecessor = context.predecessor
  storage.set("contract_owner", predecessor)
  storage.set("init", "done");
}

export function change_metadata_uri(token_id: TokenId, new_metadata_uri: string): void {
  const owner = tokenToOwner.getSome(token_id);
  assert(context.predecessor == owner, ERROR_CALLER_ID_DOES_NOT_MATCH_EXPECTATION)
  tokenToMetadata.set(token_id, new_metadata_uri);
}

export function change_subscription_price(token_id: TokenId, newPrice: u64) : void {
  const owner = tokenToOwner.getSome(token_id);
  assert(context.predecessor == owner, ERROR_CALLER_ID_DOES_NOT_MATCH_EXPECTATION)  
  for (let i = 0; i < ownerToToken.length; ++i) {
    if(ownerToToken[i].token_id == token_id) {
      let newToken = new TokenOwner(token_id, owner, newPrice, ownerToToken[i].metadata_uri);
      ownerToToken.replace(i, newToken);    
    }    
  }
}

export function grant_access(escrow_account_id: string): void {
  escrowAccess.set(context.predecessor, escrow_account_id)
}

export function revoke_access(escrow_account_id: string): void {
  escrowAccess.delete(context.predecessor)
}

export function transfer_from(owner_id: string, new_owner_id: string, token_id: TokenId): void {
  const predecessor = context.predecessor
  const owner = tokenToOwner.getSome(token_id)
  if(owner != owner_id) {
    const escrow = escrowAccess.get(owner)
    assert([owner, escrow].includes(predecessor), ERROR_CALLER_ID_DOES_NOT_MATCH_EXPECTATION)
  }  

  tokenToOwner.set(token_id, new_owner_id)
  for (let i = 0; i < ownerToToken.length; ++i) {
    if(ownerToToken[i].token_id == token_id) {
      let newToken = new TokenOwner(token_id, new_owner_id, ownerToToken[i].subscriptionPrice, ownerToToken[i].metadata_uri);
      ownerToToken.replace(i, newToken);    
    }    
  }
}

export function transfer(new_owner_id: string, token_id: TokenId): void {
  const predecessor = context.predecessor

  const owner = tokenToOwner.getSome(token_id)
  assert(owner == predecessor || owner == null, ERROR_TOKEN_NOT_OWNED_BY_CALLER)

  tokenToOwner.set(token_id, new_owner_id)
  for (let i = 0; i < ownerToToken.length; ++i) {
    if(ownerToToken[i].token_id == token_id) {
      let newToken = new TokenOwner(token_id, new_owner_id, ownerToToken[i].subscriptionPrice, ownerToToken[i].metadata_uri);
      ownerToToken.replace(i, newToken);  
    }    
  }
}

export function check_access(account_id: string): boolean {
  const caller = context.predecessor

  assert(caller != account_id, ERROR_CALLER_ID_DOES_NOT_MATCH_EXPECTATION)

  if (!escrowAccess.contains(account_id)) {
    return false
  }

  const escrow = escrowAccess.getSome(account_id)
  return escrow == caller
}

export function get_token_owner(token_id: TokenId): string {
  return tokenToOwner.getSome(token_id)
}

export function get_token_id_by_owner(account: AccountId): Array<TokenOwner> {
  let ownedByAccount = new Array<TokenOwner>();
  for (let i = 0; i < ownerToToken.length; ++i) {
    if(ownerToToken[i].owner == account) {
      ownedByAccount.push(ownerToToken[i]);
    }    
  }
  return ownedByAccount;
}

export function get_token_id_by_subscription_owner(account: AccountId): Array<SubscriptionTokenOwner> {
  let ownedByAccount = new Array<SubscriptionTokenOwner>();
  for (let i = 0; i < subscriptionOwnerToToken.length; ++i) {
    if(subscriptionOwnerToToken[i].owner == account) {
      ownedByAccount.push(subscriptionOwnerToToken[i]);
    }    
  }
  return ownedByAccount;
}

export function get_subscription_price(tokenId: TokenId): u64 {
  const owner = get_token_owner(tokenId);
  for (let i = 0; i < ownerToToken.length; ++i) {
    if(ownerToToken[i].owner == owner && ownerToToken[i].token_id == tokenId) {
      return ownerToToken[i].subscriptionPrice
    }    
  }
  return 0;
}

export function get_balance(): u128 {
  const caller = context.predecessor
  const owner = storage.getSome<string>('contract_owner')
  assert(caller == owner, ERROR_CALLER_ID_DOES_NOT_MATCH_EXPECTATION)
  return context.accountBalance;
}

export function get_token_uri(token_id: TokenId): MetadataUri {
  return tokenToMetadata.getSome(token_id)
}

export function get_contract_owner(): string | null {
  return storage.get<string>("contract_owner")
}

export function mint_to(owner_id: AccountId, metadata_uri: MetadataUri, subscription_price: u64): u64 { 
  const tokenId = storage.getPrimitive<u64>(TOTAL_SUPPLY, 1)

  tokenToOwner.set(tokenId, owner_id)

  tokenToMetadata.set(tokenId, metadata_uri)
  let newToken = new TokenOwner(tokenId, owner_id, subscription_price, metadata_uri)
  ownerToToken.push(newToken);

  storage.set<u64>(TOTAL_SUPPLY, tokenId + 1)

  const owner = storage.get<string>("contract_owner")
  assert(owner != null, ERROR_OWNER_ID_DOES_NOT_MATCH_EXPECTATION)
  return tokenId
}

export function mint_subscription_to(owner_id: AccountId, publicationTokenId: u64, metadata_uri: MetadataUri): u64 { 
  let owner = "";
  let price = u64(0);
  for (let i = 0; i < ownerToToken.length; ++i) {
    if(ownerToToken[i].token_id == publicationTokenId) {
      price = ownerToToken[i].subscriptionPrice
      owner = ownerToToken[i].owner;      
    }    
  }  
  assert(owner !== "", "Owner not set")
  assert(context.attachedDeposit >= u128.from(price), ERROR_DEPOSIT_AMOUNT_INVALID)
  
  const tokenId = storage.getPrimitive<u64>(TOTAL_SUPPLY, 1)

  tokenToOwner.set(tokenId, owner_id)

  tokenToMetadata.set(tokenId, metadata_uri)  
  let newToken = new SubscriptionTokenOwner(tokenId, owner_id, publicationTokenId, metadata_uri);
  subscriptionOwnerToToken.push(newToken);

  storage.set<u64>(TOTAL_SUPPLY, tokenId + 1)
  if(owner) {
    ContractPromiseBatch.create(owner).transfer(u128.from(context.attachedDeposit))
  }  
  return tokenId
}