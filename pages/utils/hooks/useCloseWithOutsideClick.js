import React, { useState, useEffect } from 'react'

export const useCloseWithOutsideClick = (element) => {
  const [shouldClose, setShouldClose] = useState(false);
  useEffect(() => {
    document.addEventListener("mousedown", handleClick);
    
    return () => {
      document.removeEventListener("mousedown", handleClick)
    }
  }, []);

  const handleClick = (e) => {
    const isTarget = e.target.classList.contains(element);
    if(!isTarget) {
      setShouldClose(true);
    }
  }

  return { shouldClose, setShouldClose }
}
