import Head from "next/head";
import React, { useEffect } from "react";
import Dashboard from "./components/pages/Dashboard/index";
import PrivateRoute from "./PrivateRoute";
import { connect } from "react-redux";
import { checkForSession } from "./actions/authActions";
import { initContract } from './utils/near_config'

const Home = ({ authentication, checkForSession }) => {
  useEffect(() => {
    initContract()
    // if(!authentication.wallet) {
    //   checkForSession();
    // } else {
    //   initContract()
    // }
  }, []);
  
  return (
    <div>
      <Head>
        <title>Hamlet</title>
        <meta name="description" content="Your writing, your audience" />
        <link rel="icon" href="/assets/favicon.png" />
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
          href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
          rel="stylesheet"
        />
        <link
          href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap"
          rel="stylesheet"
        />
      </Head>
      <PrivateRoute exact path="/" component={Dashboard} />
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    authentication: state.authentication
  }
}

export default connect(mapStateToProps, { checkForSession })(Home);
