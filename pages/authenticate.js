import React, { useEffect } from 'react'
import Logo from './components/Logo'
import Head from "next/head"
import { useRouter } from 'next/router'
import { connect } from "react-redux"
import { signIn } from './actions/authActions'

const Authenticate = ({ authentication: { wallet }, signIn}) => {
  const router = useRouter();
  useEffect(() => {
    if(router.query.account_id) {      
      getAccountAndSignIn();      
    }
  }, [router]);

  const getAccountAndSignIn = async () => {
    await signIn();
    router.push("/");
  }
  return (
    <div className="w-full h-screen flex flex-col justify-center items-center">
      <Head>
        <title>Hamlet</title>
        <meta name="description" content="Your writing, your audience" />
        <link rel="icon" href="/assets/favicon.png" />
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
          href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
          rel="stylesheet"
        />
        <link
          href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap"
          rel="stylesheet"
        />
      </Head>
      <Logo size="text-7xl" color="black" />
      <div>
        <h3 className="font-montserrat">Logging you in...</h3>
      </div>      
    </div>
  )
}

const mapStateToProps = (state) =>  {
  return {
    authentication: state.authentication
  }
}

export default connect(mapStateToProps, { signIn })(Authenticate);
