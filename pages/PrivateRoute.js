import React from "react";
import AuthPage from "./components/pages/Auth/AuthPage";
import { connect } from "react-redux";

const handleRender = (props, Component) => {
  if (!props.authentication.user) {
    return <AuthPage />;
  }

  return <Component {...props} />;
};

const PrivateRoute = ({ component: Component, ...props }) => {
  return <div>{handleRender(props, Component)}</div>;
};

const mapStateToProps = (state) => {
  return {
    authentication: state.authentication
  }
}

export default connect(mapStateToProps)(PrivateRoute);
