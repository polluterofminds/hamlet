import { handleNearConnection } from "./authActions";
import { returnReadableError } from "../utils/errors";
import * as nearAPI from "near-api-js";
import { LOAD_PUBLICATION_INFO, LOAD_WALLET, LOG_IN, LOG_OUT } from "./types";
import { getConfig, getEnvironment } from "../utils/near_config";
import { generateSeedPhrase, parseSeedPhrase } from "near-seed-phrase";
import axios from "axios";
import crypto from "crypto";

const signMessage = (message, networkId) => async (dispatch) => {
  const wallet = await dispatch(handleNearConnection());
  const keyStore = new nearAPI.keyStores.BrowserLocalStorageKeyStore(window.localStorage, 'near-api-js:keystore:')
  console.log(keyStore)
  const inMemorySigner = new nearAPI.InMemorySigner(keyStore)
  const signed = await inMemorySigner.signMessage(Buffer.from("hello, world"), wallet.getAccountId(), networkId);
  console.log(signed);
  // const hashed = await crypto.subtle.digest('SHA-256', tokenMessage);
  // const hash = new Uint8Array(hashed);

  // let isValid = nacl.sign.detached.verify(hash, signatureData.signature, signatureData.publicKey.data);
  // console.log(isValid);
};

export const createPublication =
  (publicationName, subscriptionPrice) => async (dispatch) => {
    try {
      //  First we need to create the record in our DB

      const wallet = await dispatch(handleNearConnection());
      await dispatch(signMessage("hello", "polluterofminds.testnet", "testnet"));
      // const contract = new nearAPI.Contract(
      //   wallet.account(), // the account object that is connecting
      //   "polluterofminds.testnet",
      //   {
      //     // name of contract you're connecting to
      //     // viewMethods: ["getMessages"], // view methods do not change state but usually return a value
      //     changeMethods: ["mint_to"], // change methods modify state
      //     sender: wallet.account(), // account object to initialize and sign transactions.
      //   }
      // );

      // const token = await contract.mint_to({
      //   owner_id: wallet.account().accountId,
      //   metadata_uri: "qm33csjdj",
      // });
      // console.log(token);
      return true;
    } catch (error) {
      console.log(error);
      alert(error.message);
    }
  };

export const loadPublicationData = (isOwner, pubName) => async (dispatch) => {
  try {
    // const wallet = await dispatch(handleNearConnection());
    // const contract = new nearAPI.Contract(
    //   wallet.account(), // the account object that is connecting
    //   "polluterofminds.testnet",
    //   {
    //     viewMethods: ["get_publication_for_owner", "get_subscription_price"], // view methods do not change state but usually return a value
    //     changeMethods: ["mint_to", "create_new_publication"], // change methods modify state
    //     sender: wallet.account(), // account object to initialize and sign transactions.
    //   }
    // );
    // console.log(wallet.account())
    // const name = await contract.get_publication_for_owner({
    //   caller: wallet.account().accountId
    // });
    // const price = await contract.get_subscription_price({
    //   publicationName: name
    // });
    // console.log({name, price})
    dispatch({
      type: LOAD_PUBLICATION_INFO,
      payload: {
        name: null,
        price: null,
        loading: false,
      },
    });
  } catch (error) {
    console.log(error);
    dispatch({
      type: LOAD_PUBLICATION_INFO,
      payload: {
        name: null,
        price: null,
        loading: false,
      },
    });
  }
};
