export const LOG_IN = 'LOG_IN';
export const LOG_OUT = 'LOG_OUT';
export const LOAD_WALLET = 'LOAD_WALLET';

export const LOAD_PUBLICATION_INFO = 'LOAD_PUBLICATION_INFO';