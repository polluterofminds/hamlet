import { returnReadableError } from "../utils/errors";
import * as nearAPI from "near-api-js";
import { LOAD_WALLET, LOG_IN, LOG_OUT } from "./types";
import { getConfig, getEnvironment } from "../utils/near_config";
import { generateSeedPhrase, parseSeedPhrase } from "near-seed-phrase";
import axios from "axios";
const TEMP_OWNER = "__TEMP_OWNER";
const GAS = "200000000000000";
const MIN_ATTACHED_BALANCE = "0";

const { connect: nearConnect, WalletConnection, KeyPair } = nearAPI;

export const handleNearConnection = () => async (dispatch) => {
  try {
    const nearConfig = getConfig(process.env.NEAR_ENV || "development");
    // connect to NEAR
    const near = await nearConnect({
      deps: {
        keyStore: new nearAPI.keyStores.BrowserLocalStorageKeyStore(),
      },
      ...nearConfig,
    });

    // create wallet connection
    const nearWallet = new WalletConnection(near);
    dispatch({
      type: LOAD_WALLET,
      payload: nearWallet,
    });
    return nearWallet;
  } catch (error) {
    returnReadableError(error, dispatch);
  }
};

export const createNewAccount = async (user, newAccountName) => {
  const accountId = `${newAccountName
    .split(" ")
    .join("_")
    .toLowerCase()}.${getEnvironment(process.env.NEAR_ENV || "development")}`;
  const nearConfig = getConfig(process.env.NEAR_ENV || "development");
  // connect to NEAR
  const near = await nearConnect({
    deps: {
      keyStore: new nearAPI.keyStores.BrowserLocalStorageKeyStore(),
    },
    ...nearConfig,
  });
  const account = await near.account(user);
  const { seedPhrase, secretKey } = generateSeedPhrase();
  const keyPair = KeyPair.fromString(secretKey);
  localStorage.setItem(
    TEMP_OWNER,
    JSON.stringify({
      seedPhrase,
      accountId,
    })
  );

  await account.functionCall(
    "testnet",
    "create_account",
    {
      new_account_id: accountId,
      new_public_key: keyPair.publicKey.toString(),
    },
    GAS,
    MIN_ATTACHED_BALANCE
  );
  await account.sendMoney(
    accountId, // receiver account
    "10000000000000" // amount in yoctoNEAR
  );
  // const account = await near.account(user);
  // console.log(account);
  // const keyPair = nearAPI.utils.KeyPair.fromRandom('ed25519');
  // console.log(`${newAccountName}.${getEnvironment(process.env.NEAR_ENV || "development")}`);
  // await account.createAccount(
  //   `${newAccountName}.${getEnvironment(process.env.NEAR_ENV || "development")}`, // new account name
  //   keyPair.publicKey.toString(), // public key for new account
  //   "10000000000000000000" // initial balance for new account in yoctoNEAR
  // );
};

export const checkForSession = () => async (dispatch) => {
  try {
    const wallet = await dispatch(handleNearConnection());
    const account = wallet.getAccountId();
    if (account) {
      dispatch({
        type: LOG_IN,
        payload: account,
      });
    } else {
      dispatch({
        type: LOG_OUT,
      });
    }
  } catch (error) {
    returnReadableError(error, dispatch);
  }
};

export const signIn = () => async (dispatch) => {
  try {
    const wallet = await dispatch(handleNearConnection());
    const account = wallet.getAccountId();
    dispatch({
      type: LOG_IN,
      payload: account,
    });
  } catch (error) {
    returnReadableError(error, dispatch);
  }
};

export const signOut = () => async (dispatch) => {
  try {
    const wallet = await dispatch(handleNearConnection());
    await wallet.signOut();
    dispatch({
      type: LOG_OUT,
    });
  } catch (error) {
    returnReadableError(error, dispatch);
  }
};
