export const lengthCheck = (str, min) => {
  return str.length >= min;
}

export const canCreatePub = (subscriptions, price) => {
  if(subscriptions) {
    return price > 0;
  }

  return true;
}