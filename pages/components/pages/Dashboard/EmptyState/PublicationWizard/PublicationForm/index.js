import React, { useRef, useEffect } from 'react'
import ButtonSecondary from '../../../../../Shared/ButtonSecondary';
import { lengthCheck } from '../utils';
import BenefitsChecks from './BenefitsChecks'

const PublicationForm = ({ setPage, name, setName }) => {
  const inputElement = useRef(null);
  useEffect(() => {
    if (inputElement.current) {
      inputElement.current.focus();
    }
  }, []);

  return (
    <div className="mb-6">
      <h3 className="font-montserrat md:text-3xl text-xl">Let's Create Your Publication</h3>
      <p className="font-open-sans text-sm text-blue-1 mt-5 mb-8">A publication is the home for your writing. You will publish your writing to your publication. Your publication also allows you full control over subscription pricing and members-only content.</p>
      <hr />
      <form className="mt-8 lg:flex lg:flex-row lg:justify-between">
        <div className="mt-3 lg:w-1/2 mb-3 mr-3">
          <p className="font-open-sans mb-3">Give It A Name</p>
          <input value={name} onChange={(e) => setName(e.target.value)} ref={inputElement} autoFocus className="max-w-md w-3/4 p-2 focus:outline-none focus:ring focus:border-input border-blue-3" placeholder="The Daily Bugle" type="text" />
          <div className="lg:mt-40">
            <ButtonSecondary disabled={!lengthCheck(name, 3)} action={() => setPage("price")} text="Next" width="w-32" />            
          </div>
        </div>    
        <div className="lg:w-1/2 bg-blue-9 p-8 lg:mt-0 mt-10 rounded-lg flex flex-col justify-center items-center">
          <div>
            <h5 className="font-montserrat text-lg">What makes for a great publication name?</h5>
            <div className="mt-4">
              <BenefitsChecks text="Easy To Remember" />
              <BenefitsChecks text="Short" />
              <BenefitsChecks text="Unique To You" />
            </div>
          </div>
        </div>    
      </form>
    </div>
  )
}

export default PublicationForm