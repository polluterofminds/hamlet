import React from "react";
import { CheckCircleIcon } from "@heroicons/react/solid"

const BenefitsChecks = ({ text }) => {
  return (
    <span className="flex flew-row py-1">
      <CheckCircleIcon className="w-8 h-8 text-green-6" /> <span className="pt-1 ml-2 font-open-sans">{text}</span>
    </span>
  );
};

export default BenefitsChecks;
