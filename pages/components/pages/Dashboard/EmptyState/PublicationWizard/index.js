import React from 'react'
import PublicationForm from './PublicationForm'
import PriceForm from './PriceForm';

const index = ({ hanldeCreatePublication, page, setPage, subscriptions, setSubscriptions, name, setName, price, setPrice, creatingPub }) => {
  switch(page) {
    case "name": 
      return <PublicationForm name={name} setName={setName} setPage={setPage} />
    case "price": 
      return <PriceForm hanldeCreatePublication={hanldeCreatePublication} price={price} setPrice={setPrice} creatingPub={creatingPub} setPage={setPage} subscriptions={subscriptions} setSubscriptions={setSubscriptions} />
    default: 
      return <PublicationForm setPage={setPage} />
  }
}

export default index
