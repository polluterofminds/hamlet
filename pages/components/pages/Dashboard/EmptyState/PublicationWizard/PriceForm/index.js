import React, { useRef, useEffect } from "react";
import ButtonPrimary from "../../../../../Shared/ButtonPrimary";
import { canCreatePub } from "../utils";
import SubscriptionForm from "./SubscriptionForm";

const PriceForm = ({ hanldeCreatePublication, setPage, subscriptions, setSubscriptions, price, setPrice, creatingPub }) => {
  const inputElement = useRef(null);
  useEffect(() => {
    if (inputElement.current) {
      inputElement.current.focus();
    }
  }, []);

  const handleCreate = async (e) => {
    e.preventDefault();
    hanldeCreatePublication();
  }

  return (
    <div>
      <div className="mb-6">
        <h3 className="font-montserrat md:text-3xl text-xl">
          Set Subscription Terms
        </h3>
        <p className="font-open-sans text-sm text-blue-1 mt-5 mb-8">
          You can require subscription payments from readers in order to read
          your writing. Simply choose whether you'd like to implement and the
          price of the subscription in NEAR tokens.
        </p>
        <hr />
        <form className="mt-8 lg:flex lg:flex-row lg:justify-between">
          <div className="mt-3 lg:w-1/2 mb-3 mr-3">
            <SubscriptionForm price={price} setPrice={setPrice} subscriptions={subscriptions} setSubscriptions={setSubscriptions} inputElement={inputElement} />
            <div className="flex flex-row justify-between">
              <div className="lg:mt-40">
                <button
                  onClick={() => setPage("name")}
                  className="focus:outline-none py-3 font-open-sans"
                >
                  Back
                </button>
              </div>
              <div className="lg:mt-40">
                <ButtonPrimary pending={creatingPub} pendingText="Creating publication..." disabled={!canCreatePub(subscriptions, price)} action={handleCreate} width="w-32" text="Create" />                
              </div>
            </div>
          </div>
          <div className="lg:w-1/2 bg-blue-9 p-8 lg:mt-0 mt-10 rounded-lg flex flex-col justify-center items-center">
            <div>
              <h5 className="font-montserrat text-lg">
                What's the current price of NEAR in USD?
              </h5>
              <div className="mt-4">NEAR PRICE CHECK</div>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default PriceForm;
