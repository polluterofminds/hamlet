import React from "react";
import Toggle from "../../../../../Shared/Toggle";

const SubscriptionForm = ({
  subscriptions,
  setSubscriptions,
  inputElement,
  price, setPrice
}) => {
  return (
    <div>
      <p className="font-open-sans mb-3">Enable Subscriptions?</p>
      <Toggle checked={subscriptions} toggle={setSubscriptions} label="On" />
      {subscriptions && (
        <div>
          <p className="font-open-sans mb-3">Subscription Price</p>
          <input
            ref={inputElement}
            autoFocus
            value={price}
            min={0}
            onChange={(e) => setPrice(e.target.value)}
            className="max-w-md w-3/4 p-2 focus:outline-none focus:ring focus:border-input border-blue-3"
            placeholder="Set your price"
            type="number"
          />
        </div>
      )}
    </div>
  );
};

export default SubscriptionForm;
