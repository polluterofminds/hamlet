import React from "react";

const CreatePublication = () => {
  return (
    <div className="flex flex-col md:w-4/6 md:mt-48 mt-24 w-full">
      <div className="container">
        <h1 className="font-montserrat lg:text-5xl text-3xl">Create a Publication</h1>
        <p className="font-open-sans lg:text-2xl text-xl mt-8 font-light">Get started by creating a publication. Start writing. Build your audience. Get paid.</p>
      </div>
    </div>
  );
};

export default CreatePublication;
