import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { signOut } from "../../../actions/authActions";
import { createPublication, loadPublicationData } from "../../../actions/publicationActions";
import Navbar from "../../Shared/Navbar";
import CreatePublication from "./EmptyState/CreatePublication";
import { PencilIcon } from "@heroicons/react/outline";
import Modal from "../../Shared/Modal";
import PublicationWizard from "./EmptyState/PublicationWizard/index";
import ButtonPrimary from "../../Shared/ButtonPrimary";
import Loading from "../../Loading";

const index = ({ signOut, authentication: { user }, publication, createPublication, loadPublicationData }) => {
  const [createModalOpen, setCreateModalOpen] = useState(false);
  const [page, setPage] = useState("name");
  const [name, setName] = useState("");
  const [subscriptions, setSubscriptions] = useState(false);
  const [price, setPrice] = useState(0);
  const [creatingPub, setCreatingPub] = useState(false);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const isOwner = true;
    loadPublicationData(isOwner);
  }, []);

  useEffect(() => {
    if(publication && publication.loading === false) {
      setLoading(false);
    }
  }, [publication])

  const handleCloseModal = () => {
    setCreateModalOpen(false);
    setPage("name");
  };

  const hanldeCreatePublication = async () => {
    try {
      await createPublication(user, name, price, "lifetime", null);
      setCreateModalOpen(false);
      setLoading(true);
      const isOwner = true;
      loadPublicationData(isOwner);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div>
      <Navbar signOut={signOut} />
      <div className="w-3/4 m-auto">
        {loading ? (
          <Loading />
        ) : publication && publication.publicationName ? 
        <div>
          <h1>{publication.publicationName}</h1>
        </div> : 
        (
          <div className="flex md:flex-row flex-col md:justify-between">
            <div>
              <CreatePublication />
              <div className="mt-10">
                <ButtonPrimary
                  text="Create Publication"
                  action={() => setCreateModalOpen(true)}
                />
              </div>
            </div>
            <div className="md:pt-56 md:flex md:flex-col md:w-1/4 md:m-auto hidden">
              <PencilIcon className="text-blue-10" />
            </div>
          </div>
        )}
      </div>
      <Modal isOpen={createModalOpen} toggle={handleCloseModal}>
        <PublicationWizard
          hanldeCreatePublication={hanldeCreatePublication}
          creatingPub={creatingPub}
          price={price}
          setPrice={setPrice}
          name={name}
          setName={setName}
          page={page}
          setPage={setPage}
          subscriptions={subscriptions}
          setSubscriptions={setSubscriptions}
        />
      </Modal>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    authentication: state.authentication,
    publication: state.publication
  };
};

export default connect(mapStateToProps, { signOut, createPublication, loadPublicationData })(index);
