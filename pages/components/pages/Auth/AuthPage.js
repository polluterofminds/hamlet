import React, { useState } from "react";
import Logo from "../../Logo";
import { REDIRECT_URI, APP_NAME, CONTRACT_NAME } from '../../../utils/near_config';
import { connect } from "react-redux";
import ButtonPrimary from "../../Shared/ButtonPrimary";

const AuthPage = ({ authentication: { wallet }}) => {
  const [signingIn, setSigningIn] = useState(false);

  const signIn = () => {
    setSigningIn(true);
    // wallet.requestSignIn(
    //   CONTRACT_NAME, 
    //   APP_NAME, 
    //   REDIRECT_URI 
    // );

      // Allow the current app to make calls to the specified contract on the
      // user's behalf.
      // This works by creating a new access key for the user's account and storing
      // the private key in localStorage.
      window.walletConnection.requestSignIn(CONTRACT_NAME, APP_NAME, REDIRECT_URI)
  
  };
  return (
    <div className="flex flex-col h-screen justify-center items-center">
      <Logo size="text-7xl" color="black" />
      <div className="mt-20 text-center">
        <h1 className="font-montserrat text-2xl md:text-5xl">
          Get Back To Writing
        </h1>
        <div className="mt-10">          
          <ButtonPrimary action={signIn} pending={signingIn} pendingText="Here we go..." text="Sign In"/>      
          <p className="mt-3 font-open-sans text-xs font-light">
            Create an account or sign in
          </p>
        </div>
        <div className="mt-20 font-open-sans font-light text-sm">
          <a
            className="text-purple-1 underline"
            href="mailto:justin@polluterofminds.com"
          >
            Contact
          </a>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    authentication: state.authentication
  }
}

export default connect(mapStateToProps)(AuthPage);
