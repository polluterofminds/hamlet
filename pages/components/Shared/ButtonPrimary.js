import React from 'react'

const ButtonPrimary = ({ pending, action, pendingText, text, width, disabled }) => {
  const DEFAULT_WIDTH = "w-52";
  return (
    <button disabled={disabled} onClick={action} className={`focus:outline-none ${!disabled && "bg-purple-8"} py-3 ${width ? width : DEFAULT_WIDTH} rounded-full text-purple-3 font-open-sans font-bold`}>{ pending ? pendingText : text}</button>
  )
}

export default ButtonPrimary
