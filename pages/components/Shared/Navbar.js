import React, { useState, useEffect } from "react";
import Logo from "../Logo";
import { ChevronDownIcon } from "@heroicons/react/outline";
import { connect } from "react-redux";
import NavDropdown from "./NavDropdown";
import Dropdown from "./Dropdown";
import { useCloseWithOutsideClick } from "../../utils/hooks/useCloseWithOutsideClick";
const navEl = "nav-menu";

const Navbar = ({ authentication: { user }, signOut}) => {
  const [showDropDown, setShowDropdown] = useState(false);
  const { shouldClose, setShouldClose } = useCloseWithOutsideClick(navEl);  
  
  useEffect(() => {
    if(shouldClose) {
      setShouldClose(false);
      setShowDropdown(false);
    }
  }, [shouldClose])

  return (
    <nav className="flex flex-row justify-between">
      <div className="mx-3 my-2">
        <Logo size="lg:text-5xl text-3xl" color="black" />
      </div>
      <button onClick={() => setShowDropdown(!showDropDown)} className="focus:outline-none bg-blue-6 mx-3 py-3 lg:px-6 px-3 my-2 rounded-full nav-menu">
        <span className="nav-menu flex flex-row lg:text-base text-sm">
          <span className="font-open-sans nav-menu">{user || "Anonymous"}</span> <ChevronDownIcon className="h-5 w-5 pt-1 nav-menu" />
        </span>
      </button>
      <Dropdown showDropDown={showDropDown}>
        <NavDropdown signOut={signOut} />
      </Dropdown>      
    </nav>
  );
};

const mapStateToProps = (state) => {
  return {
    authentication: state.authentication
  }
}

export default connect(mapStateToProps)(Navbar);
