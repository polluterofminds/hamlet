import React from 'react'
import Link from 'next/link'

const NavDropdown = ({ signOut }) => {
  return (
    <div>
      <h2 className="cursor-pointer nav-menu" onClick={signOut} aria-label="button" role="button menu-item">Sign Out</h2>
    </div>
  )
}

export default NavDropdown
