import React from "react";

const Toggle = ({ toggle, checked, label }) => {
  return (
    <div className="flex items-center justify-left w-full mb-12">
      <label htmlFor="toggleSubs" className="flex items-center cursor-pointer">
        <div className="relative">
          <input
            onChange={(e) => toggle(e.target.checked)}
            type="checkbox"
            id="toggleSubs"
            checked={checked}
            className="sr-only"
          />

          <div
            className={`block ${
              checked ? "bg-green-6" : "bg-gray-600"
            } w-14 h-8 rounded-full`}
          ></div>

          <div
            className={`absolute left-1 top-1 bg-white w-6 h-6 rounded-full transition ${
              checked
                ? "transform translate-x-full"
                : "transform translate-x-0"
            }`}
          ></div>
        </div>

        <div className="ml-3 text-gray-700 font-medium">{label}</div>
      </label>
    </div>
  );
};

export default Toggle;
