import React, { useEffect } from 'react'
import { XIcon } from "@heroicons/react/solid";

const Modal = ({ children, isOpen, toggle }) => {
  useEffect(() => {
    if(isOpen) {
      document.getElementById("modal").classList.remove("scale-0");
      document.getElementById("modal").classList.add("scale-100");
    } else {
      document.getElementById("modal").classList.remove("scale-100");
      document.getElementById("modal").classList.add("scale-0");
    }
  }, [isOpen]);
  return (
    <div className={isOpen ? "fixed z-40 top-0 left-0 w-auto h-auto flex flex-col justify-center items-center" : "hidden"}>      
      <div className="w-screen h-screen bg-black bg-opacity-50" />
      <div id="modal" className="transform transition-all duration-150 ease-out scale-0 fixed z-50 md:w-3/5 h-auto w-4/5 m-auto bg-blue-10 rounded-lg p-6">
      <div onClick={toggle} aria-label="button" className="cursor-pointer absolute top-3 right-3"><XIcon className="text-black h-5 w-5" /></div>
        <div className="md:w-3/4 m-auto mt-12 w-4/5">
          {children}
        </div>        
      </div>
    </div>
  )
}

export default Modal
