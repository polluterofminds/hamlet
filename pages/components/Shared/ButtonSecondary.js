import React from 'react'

const ButtonSecondary = ({ action, pending, pendingText, text, width, disabled }) => {
  const DEFAULT_WIDTH = "w-52";
  return (
    <button disabled={disabled} onClick={action} className={`focus:outline-none ${!disabled && "bg-blue-1"} py-3 ${width ? width : DEFAULT_WIDTH} rounded-full ${!disabled ? "text-blue-10" : "text-black"} font-open-sans font-bold`}>{ pending ? pendingText : text}</button>
  )
}

export default ButtonSecondary
