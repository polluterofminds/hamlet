import React from "react";

const Dropdown = ({ children, showDropDown }) => {
  return (
    <div
      className={
        showDropDown
          ? "font-open-sans shadow bg-white w-5 absolute w-52 right-7 mt-2 rounded-lg top-14 h-auto p-5"
          : "hidden"
      }
    >
      {children}
    </div>
  );
};

export default Dropdown;
