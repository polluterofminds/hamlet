const fs = require("fs");
const { KeyPair, utils, transactions } = require('near-api-js');

export default (req, res) => {
  // const txs = [transactions.deployContract(fs.readFileSync("./contract/build/release/hamlet.wasm"))];
  const contractBinary = fs.readFileSync("./contract/build/release/hamlet.wasm")
  res.status(200).json(contractBinary);
}