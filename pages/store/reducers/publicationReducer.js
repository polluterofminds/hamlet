import { LOAD_PUBLICATION_INFO } from '../../actions/types';

const initialState = {
  loading: true, 
  publicationName: null, 
  subscriptionPrice: null
};

const authReducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case LOAD_PUBLICATION_INFO:
      return {
        ...state, 
        publicationName: payload.name, 
        subscriptionPrice: payload.price,
        loading: false
      }    
    default:
      return state;
  }
};

export default authReducer;