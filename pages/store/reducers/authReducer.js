import { LOG_IN, LOG_OUT, LOAD_WALLET } from '../../actions/types';

const initialState = {
  user: null, 
  wallet: null
};

const authReducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case LOG_IN:
      return {
        ...state, 
        user: payload
      }
    case LOG_OUT:
      return {
        ...state, 
        user: null 
      }
    case LOAD_WALLET: 
      return {
        ...state, 
        wallet: payload
      }
    default:
      return state;
  }
};

export default authReducer;