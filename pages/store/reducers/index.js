import { combineReducers } from 'redux';
import authReducer from './authReducer';
import publicationReducer from "./publicationReducer"

const rootReducer = combineReducers({
  authentication: authReducer,
  publication: publicationReducer
});

export default rootReducer;