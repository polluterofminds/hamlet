import {createWrapper} from 'next-redux-wrapper';
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk from 'redux-thunk';
import rootReducer from './reducers';


// export const initStore = (initialState = {}) => {
//   return createStore(combineReducers, initialState, applyMiddleware(thunk));
// };

const makeStore = context => createStore(rootReducer, {}, composeWithDevTools(applyMiddleware(thunk)));

export const wrapper = createWrapper(makeStore, {debug: true});