import React from 'react'
import Logo from '../../Logo'
import AuthButton from './AuthButton'

const AuthPage = () => {
  return (
    <div className="flex flex-col h-screen justify-center items-center">
      <Logo size="7xl" color="black" />
      <div className="mt-20 text-center">
        <h1 className="font-montserrat text-2xl md:text-5xl">Get Back To Writing</h1>
        <div className="mt-10">
          <AuthButton />
          <p className="mt-3 font-open-sans text-xs font-light">Create an account or sign in</p>
        </div>        
        <div className="mt-20 font-open-sans font-light text-sm">
          <a className="text-purple-1 underline" href="mailto:justin@polluterofminds.com">Contact</a>
        </div>
      </div>
    </div>
  )
}

export default AuthPage
