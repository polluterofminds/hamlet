import React from 'react'

const AuthButton = () => {
  return (
    <button className="bg-purple-8 py-3 w-52 rounded-full text-purple-3 font-open-sans font-bold">Sign In</button>
  )
}

export default AuthButton
