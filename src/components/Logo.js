import React from 'react'

const Logo = ({ color, size }) => {
  return (
    <div>
      <h1 className={`text-${size} text-${color} font-montserrat font-bold`}>hamlet<span className="text-purple-6">.</span></h1>
    </div>
  )
}

export default Logo
