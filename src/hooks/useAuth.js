import { useState, useEffect } from "react";
import * as nearAPI from "near-api-js";
import { getConfig } from '../config';

export const useAuth = () => {
  const [isSignedIn, setIsSignedIn] = useState(false);
  useEffect(() => {
    handleNearConnection();
  }, []);

  const handleNearConnection = async () => {
    const { connect: nearConnect, WalletConnection } = nearAPI;

    const nearConfig = getConfig(process.env.NODE_ENV || "development");

    // connect to NEAR
    const near = await nearConnect(nearConfig);

    // create wallet connection
    const wallet = new WalletConnection(near);
    console.log(wallet.isSignedIn());
    setIsSignedIn(wallet.isSignedIn());
  };

  const signIn = () => {
    wallet.requestSignIn(
      "example-contract.testnet",     // contract requesting access 
      "Example App",                  // optional
      "http://YOUR-URL.com/success",  // optional
      "http://YOUR-URL.com/failure"   // optional
      );
  };

  return { isSignedIn, signIn };
};
