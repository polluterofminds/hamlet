import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Dashboard from "./components/pages/Dashboard/index";
import PrivateRoute from "./PrivateRoute";

const App = () => {
  return (
    <div>
      <BrowserRouter>
        <Switch>
          <PrivateRoute exact path="/" component={Dashboard} />
        </Switch>
      </BrowserRouter>
    </div>
  );
};

export default App;
