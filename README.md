## Hamlet

Hamlet is a publishing platform built on Next.js and NEAR Protocol. This is a very much in progress repository. 

## How to deploy your own Hamlet contract 

1. `git clone `
2. `cd hamlet-next`
3. `yarn`
4. `npm i -g near-cli`
5. `near login`
6. `near deploy --wasmFile ./out/main.wasm --accountId YOUR__ACCOUNT_ID`

## How to run the project locally

WARNING: This is still in progress

1. `git clone `
2. `cd hamlet-next`
3. `yarn`
4. `npm i -g near-cli`
5. `near login`
6. `near deploy --wasmFile ./out/main.wasm --accountId YOUR__ACCOUNT_ID`
7. `yarn dev`
